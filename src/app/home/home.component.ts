import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../core/services/character.service';
import { Character } from '../core/models/character';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  characters: Character[];

  constructor(private characterService: CharacterService) {}

  ngOnInit(): void {
    this.getCharacters();
  }

  getCharacters() {
    this.characterService.getAll().subscribe((character: Character[]) => {
      this.characters = character;
    });
  }

}
