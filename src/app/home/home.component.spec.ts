import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { CharacterService } from '../core/services/character.service';
import { Observable, Observer } from 'rxjs';
import { HttpRequest } from '@angular/common/http';
import { Character } from '../core/models/character';

let mockData = [{
  char_id: 55, 
  name: 'Stacey Ehrmantraut',
  birthday: 'Unknown',
  occupation: ['APD Officer'],
  img: 'https://vignette.wikia.nocookie.net/breakingbad/images/b/b3/StaceyEhrmantraut.png/revision/latest?cb=20150310150049',
  status: 'Alive',
  nickname: 'Mike\'s daugter-in-law',
  appearance: [3],
  portrayed: 'Kerry Condon'
}];

class MockCharacterService {
  getAll(url) {
    return Observable.create((observer: Observer<Character[]>) => {
      observer.next(mockData);
    });
  }
}

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [
        { provide: CharacterService, useClass: MockCharacterService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the characters properties' , async(() => {
    component.getCharacters();

    fixture.whenStable().then(() => {
      expect(component.characters).toEqual(mockData);
    });
  }));

  it('should validate character display', () => {
    
    component.getCharacters();

    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h5').textContent).toContain('Stacey Ehrmantraut');
    expect(compiled.querySelector('h6').textContent).toContain('Mike\'s daugter-in-law');
  });
});