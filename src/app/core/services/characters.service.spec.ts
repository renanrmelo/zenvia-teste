import { CharacterService } from './character.service';
import { Character } from '../models/character';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

const expectedCharacter: Character[] =
  [{
    char_id: 55, name: 'Stacey Ehrmantraut',
    birthday: 'Unknown',
    occupation: ['APD Officer'],
    img: 'https://vignette.wikia.nocookie.net/breakingbad/images/b/b3/StaceyEhrmantraut.png/revision/latest?cb=20150310150049',
    status: 'Alive',
    nickname: 'Mike\'s daugter-in-law',
    appearance: [3],
    portrayed: 'Kerry Condon'
  }];

describe('CharacterService', () => {

  let httpClientSpy: { get: jasmine.Spy };
  let characterService: CharacterService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    characterService = new CharacterService(<any>httpClientSpy);
  });

  it('should return expected characters', () => {

    httpClientSpy.get.and.returnValue(of(expectedCharacter));

    characterService.getAll().subscribe(
      characters => expect(characters).toEqual(expectedCharacter, 'expected character'),
      fail
    );

    expect(httpClientSpy.get.calls.count()).toBe(1);
  });

  it('should return an error when a TimeOut occurs', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'test 408 error',
      status: 408, statusText: 'TimeOut'
    });

    httpClientSpy.get.and.returnValue(of(errorResponse));

    characterService.getAll().subscribe(
      () => { },
      error => expect(error.message).toContain('Código do erro: 408, menssagem: test 408 error)')
    );
  });
});
