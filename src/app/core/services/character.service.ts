import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Character } from '../models/character';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

const GET_CHARACTERS_URL = environment.apiUrl + environment.getAllCharactersEndpoint;

@Injectable({
  providedIn: 'root'
})

export class CharacterService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Character[]> {
    return this.httpClient.get<Character[]>(GET_CHARACTERS_URL, {
      params: {
        category: environment.categoryName
      }
    }).pipe(
      catchError(this.handleError))
  }

  handleError(error: HttpErrorResponse) {
    console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = 'Código do erro: ${error.status}, ' + 'menssagem: ${error.message}';
    }
    alert(errorMessage);
    return throwError(errorMessage);
  };
}
