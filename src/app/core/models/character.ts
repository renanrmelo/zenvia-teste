export interface Character {
    char_id: number;
    name: string;
    birthday: string;
    occupation: Array<String>;
    img: string;
    status: string;
    nickname: string;
    appearance: Array<number>;
    portrayed: string;
}