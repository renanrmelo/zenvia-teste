export const environment = {
  production: true,
  apiUrl: 'https://www.breakingbadapi.com',
  getAllCharactersEndpoint: '/api/characters',
  categoryName: 'Breaking Bad'
};