export const environment = {
  production: false,
  apiUrl: 'https://www.breakingbadapi.com',
  getAllCharactersEndpoint: '/api/characters',
  categoryName: 'Breaking Bad'
};