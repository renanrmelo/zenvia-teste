
# ZenviaTeste

Este projeto foi desenvolvido utilizando [Angular](https://angular.io/). O Objetivo desta aplicação é exibir todos os personagens da série Breaking Bad, utilizando como fonte de dados o serviço [BreackingBadAPI](https://www.breakingbadapi.com/)
 
Caso você precise realizar a instalação do Angular em sua máquina, basta seguir a documentação neste [link](https://angular.io/guide/setup-local).

## Executar a aplicação localmente
 
Execute o comando`ng serve`.  Acesse em seu browser a seguinte URL `http://localhost:4200/`. 

## Running unit tests

Execute `ng test` para executar os testes unitários.